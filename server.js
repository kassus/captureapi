'use strict';

// Declare all constants

const express = require("express");
const bodyParser = require("body-parser");
const db = require("./db")();
const app = require("./app");
const server = require("http").Server(app);
const logger = require("./logging/logger");


// default options
app.use(bodyParser.urlencoded({ extended: true }));


// -------------------------------------------------------
// Start server
// -------------------------------------------------------

// logging 

logger.info("Starting TAKEANDKNOW API server...");

db.sequelize.sync({
    force: true
}).then(() => {
    app.set("port", 3000);
    server.listen(app.get("port"), () => {
        logger.info(`'TAKEANDKNOW API listening on port'${app.get("port")}`);
    });
});