'use strict';

// Declare all constants

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const logger = require("./logging/logger");
let db = null;

// Sql constant

const logSqlQueries = message => {
    logger.debug(message);
};


//function to read directories in recursiv mode
const read = (dir) =>
    fs.readdirSync(dir)
        .reduce((files, file) =>
            fs.statSync(path.join(dir, file)).isDirectory() ?
                files.concat(read(path.join(dir, file))) :
                files.concat(path.join(dir, file)), []);


module.exports = () => {
    if (!db) {
        const sequelize = new Sequelize(
            'takeandknowdb',
            'root',
            '###@@@###', {
            host: process.env.DB_HOST,
            dialect: "mysql",
            logging: logSqlQueries,

        }
        );

        db = {
            sequelize,
            Sequelize,
            models: {}
        };


        const dir = path.join(__dirname, "routes");
        read(dir)
            .filter(file => {
                return (file.indexOf('.') !== 0) && (file.slice(-9) === '.model.js');
            })
            .forEach(file => {
                const model = sequelize.import(file);
                db.models[model.name] = model;
            });

        Object.keys(db.models).forEach(function (key) {
            if ("associate" in db.models[key]) {
                db.models[key].associate(db.models);
            }
        });
    }

    return db;
};