'use strict';

// Declare all constant

const express = require("express");
const bodyParser = require("body-parser");
const Boom = require("boom");
const path = require("path");
const cors = require("cors");
const multer = require('multer');

// SET STORAGE
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})

var upload = multer({ storage: storage });


// Create Express app
const app = express();

app.use(
    bodyParser.urlencoded({
        extended: true,
        limit: "50mb"
    })
);

app.use(bodyParser.raw());

app.use(cors());

const apiRoutes = require("./routes/routes");

const httpLogger = require("./logging/http-logger");


// HTTP Logger middleware
app.use(httpLogger);

// API resources routes
app.use("/api", apiRoutes);

app.get('/favicon.ico', (req, res) => res.status(204));



// API route not found handler
app.use(function (req, res, next) {
    if (!req.route)
        return next(
            Boom.notFound("Route " + req.method + " " + req.path + " not found.")
        );
    next();
});

// Function to create a Media

app.route('/send')
    .post(function () {
        return
        upload.single(), (res, req, next, err) => {
            if (err) {
                return res.end("Error uploading file.");
            }
            const formData = req.body;
            console.log('form data', formData);
            res.sendStatus(200);
            res.end("File is uploaded");
        }
    }
    );

module.exports = app;