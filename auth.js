'use strict';

// Declare all constants

const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const Boom = require("boom");

const db = require("./db")();
const User = db.models.user;

let auth = null;

// Declare JwtStrategy constant

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    algorithms: ["HS256"],
    secretOrKey: 'test api'
};


module.exports = () => {
    if (!auth) {
        const strategy = new JwtStrategy(options, (payload, callback) => {
            User.find({
                where: {
                    id: payload.id
                }
            }).then(user => {
                logger.info(user.toJSON())

                // User not found
                if (!user) {
                    return callback(Boom.unauthorized("JWT: User not found"));
                }

                return callback(null, user);
            });
        });

        passport.use(strategy);

        auth = {
            initialize: () => {
                return passport.initialize();
            },
            authenticate: () => {
                log.info("Begin authentication in auth.js");
                return passport.authenticate("jwt", {
                    session: false
                });
            }
        };
    }

    return auth;
};