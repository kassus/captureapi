'use strict';

// Declare all constants

const express = require("express");
const router = express.Router({
    mergeParams: true
});

// Get point controller
const pointController = require("./point.controller");

router.get("/findAll",
    pointController.findAll,
    pointController.responseFindAllPoints
);

router.get("/findPoiById",
    pointController.findPointById,
    pointController.responseFindPointById
);

router.post("/createPoint",
    pointController.createPoint,
    pointController.responseCreatedPoint
);


router.put("/updatePoint/:id",
    pointController.updatePoint,
    pointController.responseUpdatedPoint
);


router.delete("/deletePoint/:id",
    pointController.deletePoint,
    pointController.responseDeletedPoint
);


module.exports = router;