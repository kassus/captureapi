
'use strict';

// Declare all constants

const db = require("../../db")();
const Point = db.models.point;
const logger = require("../../logging/logger");

// Create Point schema

const pointSchema = {
    type: "object",
    properties: {
        name: {
            type: "string"
        },
        description: {
            type: "string"
        },
        lat: {
            type: "string"
        },
        lng: {
            type: "string"
        },
        poiPoint: {
            type: "string"
        },
        createdDate: {
            type: Date, default: Date.now()
        },
        updatedDate: {
            type: Date, default: ""
        }


    },
    required: ["name", "description", "lat", "lng", "poiPoint"]
}


// Function to check the Point schema

module.exports.checkPointSchema = (req, res, next) => {
    const body = req.body;

    if (tv4.validate(body, pointSchema)) {
        next();
    } else {
        next(boom.badData(tv4.error));
    }
};

// Function to get all existing Points

module.exports.findAll = (req, res, next) => {
    Point.findAll({

    }).then(
        function (points) {
            res.locals.pointsFromDb = points;
            return next();
        },
        err => {
            next(err);
        }
    );

};


module.exports.findPointById = (req, res, next) => {

    if (req.params.pointId) {
        Point.findAll({

            where: { id: req.params.pointId },

        })
            .then(function (point) {
                res.locals.pointFromDb = point;
                return next()
            },
                err => {
                    next(err);
                }
            )
    }
};

// Function to create a Point

module.exports.createPoint = (req, res, next) => {
    Point.create({
        name: req.body.name,
        description: req.body.description,
        poiPoint: req.body.poiPoint,
        lat: req.body.lat,
        lng: req.body.lng

    }).then(function (point) {
        res.locals.pointFromDb = point;
        return next()
    },
        err => {
            next(err);
        }
    );
};

// Function to update an existing Point

module.exports.updatePoint = (req, res, next) => {
    Poi.update({
        name: req.body.name,
        description: req.body.description,
        poiPoint: req.body.poiPoint,
        lat: req.body.lat,
        lng: req.body.lng
    }, {
        where: {
            id: req.params.id
        }
    })
        .then(function (point) {
            res.locals.updatedPointFromDb = point;
            return next()
        },
            err => {
                next(err);
            }
        )
};

// Function to delete an existing Point

module.exports.deletePoint = (req, res, next) => {
    Point.destroy({
        where: {
            id: req.params.id
        }
    }),
        err => {
            next(err);
        }
};


// All request responses functions

module.exports.responseFindAllPoints = (req, res, next) => {
    return res.json(res.locals.pointsFromDb);
}

module.exports.responseFindPointById = (req, res, next) => {
    return res.json(res.locals.pointFromDb);
}

module.exports.responseCreatedPoint = (req, res, next) => {
    return res.json(res.locals.pointFromDb);
}

module.exports.responseUpdatedPoint = (req, res, next) => {
    return res.json(res.locals.updatedPointFromDb);
}

module.exports.responseDeletedPoint = (req, res, next) => {
    return res.json(res.locals.deletedPointFromDb);
}