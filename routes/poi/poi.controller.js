
'use strict';

// Declare all constants

const db = require("../../db")();
const Poi = db.models.poi;
const logger = require("../../logging/logger");

// Create Poi schema

const poiSchema = {
    type: "object",
    properties: {
        name: {
            type: "string"
        },
        description: {
            type: "string"
        },
        lat: {
            type: "string"
        },
        lng: {
            type: "string"
        },
        media: {
            type: "string"
        },
        createdDate: {
            type: Date, default: Date.now()
        },
        updatedDate: {
            type: Date, default: ""
        }


    },
    required: ["name", "description", "lat", "lng", "media"]
}


// Function to check the Poi schema

module.exports.checkPoiSchema = (req, res, next) => {
    const body = req.body;

    if (tv4.validate(body, poiSchema)) {
        next();
    } else {
        next(boom.badData(tv4.error));
    }
};

// Function to get all existing Pois

module.exports.findAll = (req, res, next) => {
    Poi.findAll({

    }).then(
        function (pois) {
            res.locals.poisFromDb = pois;
            return next();
        },
        err => {
            next(err);
        }
    );

};

// function to get Poi by his Id

module.exports.findPoiById = (req, res, next) => {

    if (req.params.poiId) {
        Poi.findAll({

            where: { id: req.params.poiId },

        })
            .then(function (poi) {
                res.locals.poiFromDb = poi;
                return next()
            },
                err => {
                    next(err);
                }
            )
    }
};



// Function to create a Poi

module.exports.createPoi = (req, res, next) => {
    Poi.create({
        name: req.body.name,
        description: req.body.description,
        media: req.body.media,
        lat: req.body.lat,
        lng: req.body.lng

    }).then(function (poi) {
        res.locals.poiFromDb = poi;
        return next()
    },
        err => {
            next(err);
        }
    );
};

// Function to update an existing Poi

module.exports.updatePoi = (req, res, next) => {
    Poi.update({
        name: req.body.name,
        description: req.body.description,
        media: req.body.media,
        lat: req.body.lat,
        lng: req.body.lng
    }, {
        where: {
            id: req.params.id
        }
    })
        .then(function (poi) {
            res.locals.updatedPoiFromDb = poi;
            return next()
        },
            err => {
                next(err);
            }
        )
};


// Function to get the seven last poi image

module.exports.getLastRecentPoiImage = (req, res, next) => {
    Poi.findAll({

    }).then(
        function (pois) {
           
            let galleriaArray = [];
            let dateToCompare = Date.now() - 216000;
            pois.forEach(poi => {
                if (poi.media != undefined && poi.media.mine == 'png') {
                    if (poi.createdDate == dateToCompare) {
                        galleriaArray.push(poi.media);
                        res.locals.recentPoiImageFromDb = galleriaArray;
                    }
                }
            });
            return next();
        },
        err => {
            next(err);
        }
    );

};

// Function to delete an existing Poi

module.exports.deletePoi = (req, res, next) => {
    Poi.destroy({
        where: {
            id: req.params.id
        }
    }),
        err => {
            next(err);
        }
};


// All request responses functions

module.exports.responseFindAllPois = (req, res, next) => {
    return res.json(res.locals.poisFromDb);
}

module.exports.responseFindPoiById = (req, res, next) => {
    return res.json(res.locals.poiFromDb);
}

module.exports.responseRecentPoiImages = (req, res, next) => {
    return res.json(res.locals.recentPoiImageFromDb);
}

module.exports.responseCreatedPoi = (req, res, next) => {
    return res.json(res.locals.poiFromDb);
}

module.exports.responseUpdatedPoi = (req, res, next) => {
    return res.json(res.locals.updatedPoiFromDb);
}

module.exports.responseDeletedPoi = (req, res, next) => {
    return res.json(res.locals.deletedPoiFromDb);
}