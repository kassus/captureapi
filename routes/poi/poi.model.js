
'use strict';

//FIXME: add created and updated date in the models

// Create POI model

module.exports = (sequelize, DataType) => {
    const Poi = sequelize.define(
        "poi", {
        id: {
            type: DataType.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,

        },
        name: {
            type: DataType.STRING,
            allowNull: false
        },
        description: {
            type: DataType.TEXT,
            allowNull: false
        },
        lat: {
            type: DataType.STRING,
            allowNull: false
        },
        lng: {
            type: DataType.STRING,
            allowNull: false
        },
        media: {
            type: DataType.STRING,
            allowNull: false
        },

    }, {
        timestamps: false,

    });

    return Poi;
};