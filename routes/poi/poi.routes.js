'use strict';

// Declare all constants

const express = require("express");
const router = express.Router({
    mergeParams: true
});

// Get media controller
const poiController = require("./poi.controller");

router.get("/findAll",
    poiController.findAll,
    poiController.responseFindAllPois
);


router.get("/findPoiById",
    poiController.findPoiById,
    poiController.responseFindPoiById
);


router.get("/getLastRecentPoiImage",
    poiController.getLastRecentPoiImage,
    poiController.responseRecentPoiImages
);

router.post("/createPoi",
    poiController.createPoi,
    poiController.responseCreatedPoi
);


router.put("/updatePoi/:id",
    poiController.updatePoi,
    poiController.responseUpdatedPoi
);


router.delete("/deletePoi/:id",
    poiController.deletePoi,
    poiController.responseDeletedPoi
);


module.exports = router;