'use strict';

// Declare all constants

const express = require("express");
const router = express.Router({
    mergeParams: true
});

// Get user controller
const userController = require("./user.controller");

router.get("/findAll",
    userController.findAll,
    userController.responseFindAllUsers
);


router.post("/signin",
    userController.findByEmail,
    userController.responseFindUserByEmail
);

router.get("findById/:id",
    userController.findById,
    userController.responseFindUserById
);

router.post("/createUser",
    userController.createUser,
    userController.responseCreatedUser
);

router.post("/signup",
    userController.createUser,
    userController.responseCreatedUser
);

router.put("updateUser/:id",
    userController.updateUser,
    userController.responseUpdatedUser
);

router.delete("deleteUser/:id",
    userController.deleteUser,
    userController.responseDeletedUser
);


module.exports = router;