
'use strict';

// Declare all constants

const db = require("../../db")();
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);
const hash = bcrypt.hashSync("B4c0/\/", salt);
const User = db.models.user;
const logger = require("../../logging/logger");

// Create User schema

const userSchema = {
    type: "object",
    properties: {
        lastname: {
            type: "string"
        },
        firstname: {
            type: "string"
        },
        login: {
            type: "string"
        },
        email: {
            type: "string"
        },
        password: {
            type: "string"
        },
        createdDate: {
            type: Date, default: Date.now()
        },
        updatedDate: {
            type: Date, default: ""
        }

    },
    required: ["firstname", "lastname", "login", "password", "email"]
}


// Function to check the User schema

module.exports.checkUserSchema = (req, res, next) => {
    const body = req.body;

    if (tv4.validate(body, userSchema)) {
        next();
    } else {
        next(boom.badData(tv4.error));
    }
};

// Function to get all existing Users

module.exports.findAll = (req, res, next) => {
    User.findAll({

    }).then(
        function (users) {
            res.locals.usersFromDb = users;
            return next();
        },
        err => {
            next(err);
        }
    );

};

module.exports.findById = (req, res, next) => {

    if (req.params.userId) {
        User.findAll({

            where: { id: req.params.userId },

        })
            .then(function (user) {
                res.locals.userFromDb = user;
                return next()
            },
                err => {
                    next(err);
                }
            )
    }
};

// find By email method

module.exports.findByEmail = (req, res, next) => {
    if (req.body.email) {
        User.findAll({

            where: {
                email: req.body.email
            },
        })
            .then(function (user) {
                res.locals.userByEmailFromDb = user;
                return next()
            },
                err => {
                    next(err);
                }
            )
    }
};

//FIXME: refactor the validation of the password

exports.validJWTNeeded = (req, res, next) => {
    if (req.headers['authorization']) {
        try {
            let authorization = req.headers['authorization'].split(' ');
            if (authorization[0] !== 'Bearer') {
                return res.status(401).send();
            } else {
                req.jwt = jwt.verify(authorization[1], secret);
                return next();
            }
        } catch (err) {
            return res.status(403).send();
        }
    } else {
        return res.status(401).send();
    }
};
// Function to create a User

module.exports.createUser = (req, res, next) => {
    User.create({
        login: req.body.login,
        password: req.body.password,
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        email: req.body.email,
        createdDate: req.body.createdDate,
        updatedDate: req.body.updatedDate
    }).then(function (user) {
        res.locals.userFromDb = user;
        return next()
    },
        err => {
            next(err);
        }
    );
};

// Function to update an existing User

module.exports.updateUser = (req, res, next) => {
    User.update({
        login: req.body.login,
        password: req.body.password,
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        email: req.body.email,
        createdDate: req.body.createdDate,
        updatedDate: req.body.updatedDate
    }, {
        where: {
            id: req.params.id
        }
    })
        .then(function (user) {
            res.locals.updatedUserFromDb = user;
            return next()
        },
            err => {
                next(err);
            }
        )
};

// Function to delete an  existing User

module.exports.deleteUser = (req, res, next) => {
    User.destroy({
        where: {
            id: req.params.id
        }
    }),
        err => {
            next(err);
        }
};


// All request responses functions

module.exports.responseFindAllUsers = (req, res, next) => {
    return res.json(res.locals.usersFromDb);
}
module.exports.responseCreatedUser = (req, res, next) => {
    return res.json(res.locals.userFromDb);
}

module.exports.responseFindUserById = (req, res, next) => {
    return res.json(res.locals.userFromDb);
}

module.exports.responseFindUserByEmail = (req, res, next) => {
    return res.json(res.locals.userByEmailFromDb);
}

module.exports.responseUpdatedUser = (req, res, next) => {
    return res.json(res.locals.updatedUserFromDb);
}

module.exports.responseDeletedUser = (req, res, next) => {
    return res.json(res.locals.deletedUserFromDb);
}