
'use strict';

//FIXME: add created and updated date in the models

// Declare bcrypt constant

const bcrypt = require('bcrypt');

// Create User model

module.exports = (sequelize, DataType) => {
    const User = sequelize.define(
        "user", {
        id: {
            type: DataType.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,

        },
        lastname: {
            type: DataType.STRING,
            allowNull: false
        },
        firstname: {
            type: DataType.STRING,
            allowNull: false
        },
        login: {
            type: DataType.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: DataType.STRING,
            allowNull: false
        },
        email: {
            type: DataType.STRING,
            allowNull: false
        },

    }, {
        timestamps: false,

    });

    return User;
};