
'use strict';

// Declare all constants

const express = require("express");
const app = require("../app");
const router = express.Router({
    mergeParams: true
});

// Declare logger constant
const logger = require("../logging/logger");

//Declare all Api routes
router.use('/user', require('./user/user.routes'));
router.use('/media', require('./media/media.routes'));
router.use('/point', require('./point/point.routes'));
router.use('/poi', require('./poi/poi.routes'));
router.use('/userProfile', require('./userProfile/userProfile.routes'));


module.exports = router;
