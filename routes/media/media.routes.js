'use strict';

const multer = require('multer');
const fs = require('fs-extra');


// Declare all constants

const express = require("express");
const router = express.Router({
    mergeParams: true
});

// Get media controller
const mediaController = require("./media.controller");


router.get("/findAll",
    mediaController.findAll,
    mediaController.responseFindAllMedias
);

router.get("/findMediaById",
    mediaController.findMediaById,
    mediaController.responseFindMediaById
);

router.put("/updateMedia/:id",
    mediaController.updateMedia,
    mediaController.responseUpdatedMedia
);


router.delete("/deleteMedia/:id",
    mediaController.deleteMedia,
    mediaController.responseDeletedMedia
);

module.exports = router;