
'use strict';


//FIXME: add created and updated date in the models

// Create MEDIA model

module.exports = (sequelize, DataType) => {
    const Media = sequelize.define(
        "media", {
        id: {
            type: DataType.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true

        },
        name: {
            type: DataType.STRING,
            allowNull: false
        },
        description: {
            type: DataType.TEXT,
            allowNull: false
        },
        imageUrl: {
            type: DataType.STRING,
            allowNull: false
        },
        videoUrl: {
            type: DataType.STRING,
            allowNull: true
        },
        soundUrl: {
            type: DataType.STRING,
            allowNull: true
        },

    }, {
        timestamps: false

    });

    return Media;
};