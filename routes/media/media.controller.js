
'use strict';

// Declare all constants

const db = require("../../db")();
const Media = db.models.media;
const logger = require("../../logging/logger");


// Create Media schema

const mediaSchema = {
    type: "object",
    properties: {
        name: {
            type: "string"
        },
        description: {
            type: "string"
        },
        imageUrl: {
            type: "string"
        },
        videoUrl: {
            type: "string"
        },
        soundUrl: {
            type: "string"
        },
        createdDate: {
            type: Date, default: Date.now()
        },
        updatedDate: {
            type: Date, default: ""
        }


    },
    required: ["name", "description", "imageUrl"]
}

// Function to get all existing Medias

module.exports.upload = () => {
    upload.single('myFile') = (req, res, next) => {
        const file = req.file
        if (!file) {
            const error = new Error('Please choose files')
            error.httpStatusCode = 400
            return next(error)
        }
        res.locals.mediaFileFromDb = file;
        res.send().status(200);

    }
};


// Function to check the Media schema

module.exports.checkMediaSchema = (req, res, next) => {
    const body = req.body;

    if (tv4.validate(body, mediaSchema)) {
        next();
    } else {
        next(boom.badData(tv4.error));
    }
};

// Function to get all existing Medias

module.exports.findAll = (req, res, next) => {
    Media.findAll({

    }).then(
        function (medias) {
            res.locals.mediasFromDb = medias;
            return next();
        },
        err => {
            next(err);
        }
    );

};

module.exports.findMediaById = (req, res, next) => {

    if (req.params.mediaId) {
        Media.findAll({

            where: { id: req.params.mediaId },

        })
            .then(function (media) {
                res.locals.mediaFromDb = media;
                return next()
            },
                err => {
                    next(err);
                }
            )
    }
};

// Function to update an existing Media

module.exports.updateMedia = (req, res, next) => {
}


// Function to delete an existing Media


module.exports.deleteMedia = (req, res, next) => {
    var mediaId = req.params.id
    Media.destroy({

        where: {
            id: mediaId
        }
    })
        .then(function (media) {
            return next()
        }).catch(err => {
            return next(err);
        });
};


// All request responses functions

module.exports.responseFindAllMedias = (req, res, next) => {
    return res.json(res.locals.mediasFromDb);
}

module.exports.responseFindMediaById = (req, res, next) => {
    return res.json(res.locals.mediaFromDb);
}

module.exports.responseCreatedMedia = (req, res, next) => {
    return res.json(res.locals.mediaCreatedFromDb);
}

module.exports.responseUpdatedMedia = (req, res, next) => {
    return res.json(res.locals.mediaUpdatedFromDb);
}

module.exports.responseDeletedMedia = (req, res, next) => {
    return res.status(200).send();
}
module.exports.responseMediasFiles = (req, res, next) => {
    return res.send(res.locals.mediaFileFromDb);
}


