
'use strict';

// Create UserProfile model

module.exports = (sequelize, DataType) => {
    const UserProfile = sequelize.define(
        "userProfile", {
        id: {
            type: DataType.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,

        },
        image: {
            type: DataType.STRING,
            allowNull: false
        },
        name: {
            type: DataType.STRING,
            allowNull: false
        },
        description: {
            type: DataType.TEXT,
            allowNull: false
        },
        email: {
            type: DataType.STRING,
            allowNull: false
        },
        attachPois: {
            type: DataType.STRING,
            allowNull: false
        },
    }, {
        timestamps: false,

    });

    return UserProfile;
};