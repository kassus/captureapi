
'use strict';

// Declare all constants

const db = require("../../db")();
const UserProfile = db.models.userProfile;
const logger = require("../../logging/logger");

// Create UserProfile schema

const userProfileSchema = {
    type: "object",
    properties: {
        image: {
            type: "string"
        },
        name: {
            type: "string"
        },
        description: {
            type: "string"
        },
        email: {
            type: "string"
        },
        attachPois: {
            type: "string"
        },
        createdDate: {
            type: Date, default: Date.now()
        },
        updatedDate: {
            type: Date, default: ""
        }
    },
    required: ["image", "name", "description", "email"]
}


// Function to check the userProfile schema

module.exports.checkUserProfileSchema = (req, res, next) => {
    const body = req.body;

    if (tv4.validate(body, userProfileSchema)) {
        next();
    } else {
        next(boom.badData(tv4.error));
    }
};

// Function to get all existing userProfile

module.exports.findAll = (req, res, next) => {
    UserProfile.findAll({

    }).then(
        function (userProfile) {
            res.locals.userProfilesFromDb = userProfile;
            return next();
        },
        err => {
            next(err);
        }
    );

};

module.exports.findUserById = (req, res, next) => {

    if (req.params.userId) {
        UserProfile.findAll({

            where: { id: req.params.userId },

        })
            .then(function (user) {
                res.locals.userProfileFromDb = user;
                return next()
            },
                err => {
                    next(err);
                }
            )
    }
};

// Function to create a UserProfile

module.exports.createUserProfile = (req, res, next) => {
    UserProfile.create({
        image: req.body.image,
        name: req.body.name,
        description: req.body.description,
        email: req.body.email,
        attachPois: req.body.attachPois

    }).then(function (userProfile) {
        res.locals.userProfileFromDb = userProfile;
        return next()
    },
        err => {
            next(err);
        }
    );
};

// Function to update an existing UserProfile

module.exports.updateUserProfile = (req, res, next) => {
    UserProfile.update({
        image: req.body.image,
        name: req.body.name,
        description: req.body.description,
        email: req.body.email,
        attachPois: req.body.attachPois
    }, {
        where: {
            id: req.params.id
        }
    })
        .then(function (userProfile) {
            res.locals.updatedUserProfileFromDb = userProfile;
            return next()
        },
            err => {
                next(err);
            }
        )
};

// Function to delete an existing UserProfile

module.exports.deleteUserProfile = (req, res, next) => {
    UserProfile.destroy({
        where: {
            id: req.params.id
        }
    }),
        err => {
            next(err);
        }
};


// All request responses functions

module.exports.responseFindAllUserProfiles = (req, res, next) => {
    return res.json(res.locals.userProfilesFromDb);
}

module.exports.responseFindUserById = (req, res, next) => {
    return res.json(res.locals.userProfileFromDb);
}
module.exports.responseCreatedUserProfile = (req, res, next) => {
    return res.json(res.locals.userProfileFromDb);
}

module.exports.responseUpdatedUserProfile = (req, res, next) => {
    return res.json(res.locals.updatedUserProfileFromDb);
}

module.exports.responseDeletedUserProfile = (req, res, next) => {
    return res.json(res.locals.deletedUserProfileFromDb);
}