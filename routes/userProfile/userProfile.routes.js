'use strict';

// Declare all constants

const express = require("express");
const router = express.Router({
    mergeParams: true
});

// Get UserProfile controller
const userProfileController = require("./userProfile.controller");

router.get("/findAll",
    userProfileController.findAll,
    userProfileController.responseFindAllUserProfiles
);

router.get("/findUserById",
    userProfileController.findUserById,
    userProfileController.responseFindUserById
);

router.post("/createUserProfile",
    userProfileController.createUserProfile,
    userProfileController.responseCreatedUserProfile
);


router.put("/updateUserProfile/:id",
    userProfileController.updateUserProfile,
    userProfileController.responseUpdatedUserProfile
);


router.delete("/deleteUserProfile/:id",
    userProfileController.deleteUserProfile,
    userProfileController.responseDeletedUserProfile
);


module.exports = router;